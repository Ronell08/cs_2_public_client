let loginForm = document.querySelector("#loginUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value 
	let password = document.querySelector("#password").value

	if(email == "" || password == "") {
		alert("Please input email and/or password!")
	}else{
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				console.log(data)

			if(data.access){
				localStorage.setItem('token', data.access);
			fetch(`http://localhost:4000/api/users/details`, {
				headers: {
					Authorization: `Bearer ${data.access}`
				}
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				localStorage.setItem("id", data._id)
				localStorage.setItem("isAdmin", data.isAdmin)
				window.location.replace("./profile.html")
			})
				
			}else {
				alert("Something went Wrong, Check your credentials!")
			}
			})
	}
})
